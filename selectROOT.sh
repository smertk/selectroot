RootBuildsDir=
Builds=()
Key=
Ans= 

cleanUp() {
	unset RootBuildsDir
	unset Builds
	unset Key
	unset Ans
}

if [[ $0 != "bash" ]]; then
	echo "**************************************************"
	echo " Please source this file, don't execute "
	echo " Like this:"
	echo
	echo " source ROOTselector"
	echo 
	echo "**************************************************"
	exit 1
fi

if ! [ -f ~/.ROOTselector ]; then
	while true; do
		read -e -p "Please enter the full path to the directory where your ROOT builds reside in: " RootBuildsDir
		if [ ! -d ${RootBuildsDir} ]; then
			echo No such directory!
			continue
		else
			clear
			echo The directory ${RootBuildsDir} contains
			ls -1 ${RootBuildsDir}
			read -e -p "Is this correct? [y,N] " Ans
			if [[ ${Ans} == "y" ]] || [[ ${Ans} == "Y" ]]; then
				echo ${RootBuildsDir} > ~/.ROOTselector
				break
			fi
		fi
	done
fi

if [ -f ~/.ROOTselector ]; then
	RootBuildsDir=`cat ~/.ROOTselector`
	echo "ROOT Builds directory is set as ${RootBuildsDir}"
	echo
	Builds=(`ls ${RootBuildsDir}`)
	echo "Found ${#Builds[@]} builds. Select the one you want to use"
	echo
	
	for i in `seq ${#Builds[@]}`;
	do
		echo "($i)"" "${Builds[$((i-1))]}
	done
    echo "(q) quit"
fi

while true; do
	read -e -p " ? " Key
	if [[ ${Key} == 'q' ]] || [[ ${KEY} == "Q" ]]; then
		cleanUp
		break
	fi
	if ! [ "$Key" -eq "$Key" ] 2>/dev/null; then
		echo Not a valid option
		continue
	fi
	if [ ${Key} -ge 1 -a ${Key} -le ${#Builds[@]} ]; then
		echo ${Builds[${Key}-1]}" Selected"
		source ${RootBuildsDir}${Builds[${Key}-1]}"/bin/thisroot.sh"
		echo ROOTSYS=$ROOTSYS
		echo LD_LIBRARY_PATH=$LD_LIBRARY_PATH
		cleanUp
		break
	else
		echo Not a valid option
	fi
done

